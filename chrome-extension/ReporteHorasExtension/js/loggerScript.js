function logHours() {
    var data = {};
    data.sessionCookie = document.cookie;

    $.ajax({
        url: 'http://localhost:3000/hourslogger',
        type: 'POST',
        data: data,
        success: function (data) { alert(`Horas logueadas:\n${data}`); },
        error: function (error) { alert(`ERROR!\n${error.responseText}`); }
    });

}

function importHoursFromCsv() {

    var formData = new FormData(document.querySelector('form[id="import-hours-form"]'));

    // first upload file
    $.ajax({
        url: 'http://localhost:3000/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) { logHours(); }, // call logger
        error: function (error) { alert(`Error subiendo el archivo:\n${JSON.stringify(error)}`); }
    });
}

var divRight = document.querySelector('div[id="divRight"]');

var importDiv = document.createElement('div');
importDiv.innerHTML = '<form id="import-hours-form"> <input class="import-input" name="csv_file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" id="load-csv-btn"/> </form> <input class="import-input" value="Importar Horas" type="button" id="import-csv-btn"/>';


importDiv.querySelector('input[id="import-csv-btn"').addEventListener('click', importHoursFromCsv, true);


divRight.insertAdjacentElement('afterend', importDiv);

