import axios from 'axios';
import FormData from 'form-data';
import config from '../config.js';
import mapJiraToRHLog from '../utils/hours-mapper.js';
import getModuleForLog from '../html-parsers/modules-parser.js';

export default class ReporteHorasRequester {
    constructor(cookies) {
        this.sessionCookies = cookies;
    }

    // login request and sets the sessionCookies to use on further requests.
    async login() {
        let formData = new FormData();
        formData.append('txtUser', config.reporte_horas.username);
        formData.append('txtPass', config.reporte_horas.password);
        formData.append('btnLogin', 'Login');

        let response = '';
        let headers = formData.getHeaders();

        try {
            response = await axios.post(config.reporte_horas.LOGIN_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });
        } catch (error) {
            console.error(`Couldn't log to Reporte Horas: ${error}`);
        }


        console.log(response.data);

        // parse cookies from login response and set it for further use
        let cookies = '';
        let cookie = response.headers['set-cookie'][0];
        cookie = cookie.split(';');
        cookies += cookie[0];

        return cookies;
    }

    async logHour(issueData) {
        let formData = new FormData();
        for (let key in issueData) {
            formData.append(key, issueData[key]);
        }
        let headers = formData.getHeaders();
        headers.Cookie = this.sessionCookies;

        let response = '';

        try {
            response = await axios.post(config.reporte_horas.REPORTE_HORAS_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });
        } catch (error) {
            throw new Error(`Couldn't log on Report Horas. ${error}`);
        }
    }

    async getModule(logData) {
        try {
            let formData = new FormData();
            formData.append('tasks', '1');
            formData.append('selProy', config.reporte_horas.project_id);
            formData.append('navegador', '0');

            let headers = formData.getHeaders();
            headers.Cookie = this.sessionCookies;

            let response = '';

            response = await axios.post(config.reporte_horas.CARGAR_COMBO_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });

            return getModuleForLog(response.data, logData.epic, logData.component, logData.type);
        } catch (error) {
            throw `Couldn't get module. ${error}`;
        }
    }

    // maps CSV to ReporteHoras data
    getIssueData(data) {
        return mapJiraToRHLog(data);
    }

    async reportHour(data, mantisIssueId) {
        try {
            if (!this.sessionCookies) {
                this.sessionCookies = await this.login();
            }

            const issueModule = await this.getModule(data);
            data.module = issueModule;
            data.issueId = mantisIssueId;

            const hourData = this.getIssueData(data);

            await this.logHour(hourData);
        }
        catch (error) {
            throw error;
        }

    }
}