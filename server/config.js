let config = {};

// Mantis
config.mantis = {
    username: 'llistorti',
    password: 'abc!123',
    project_id: '262', // 262: vtours2020

    // URLS
    LOGIN_URL: 'https://mantis.temperies.com/login.php',
    VIEW_ALL_URL: 'https://mantis.temperies.com/view_all_set.php',
    BUG_REPORT_PAGE_URL: 'https://mantis.temperies.com/bug_report_page.php',
    BUG_REPORT_URL: 'https://mantis.temperies.com/bug_report.php'
}

// Reporte Horas
config.reporte_horas = {
    username: 'llistorti',
    password: 'abc!123',
    project_id: '235', // 235: vtours2020

    // URLS
    LOGIN_URL: 'https://reportehoras.temperies.com/login.php',
    REPORTE_HORAS_URL: 'https://reportehoras.temperies.com/reporteHoras.php',
    CARGAR_COMBO_URL: 'https://reportehoras.temperies.com/cargarComboModulos.php'
}

config.csv_path = 'llistorti.csv';
config.csv_columns = ['project', 'type', 'key', 'title', 'component', 'epic', 'estimation', 'priority', 'date', 'user', 'time_spent', 'comment'];

export default config;