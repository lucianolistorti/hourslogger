import parseDataCsv from './utils/data-parser.js';
import MantisRequester from './mantis-requests/mantis-requester.js';
import ReporteHorasRequester from './reporte-horas-requests/reporte-horas-requester.js';


export default class HoursLogger {

    constructor(){
        this.csvFile = null;
    }

    /*
    *  Iterate over the hours and log them on reportehoras. Check if issues are already created, if not, create the issue on Mantis.
    */
    async logHours(rhSessionCookie) {
        const mantisRequester = new MantisRequester();
        const reporteHorasRequester = new ReporteHorasRequester(rhSessionCookie);
        let hours = await parseDataCsv(this.csvFile.path);

        let createdIssues = []; // name: issue name, issueId: id on mantis
        let errors = '';
        let loggedHours = '';

        for (let hour of hours) {
            try {
                let createdIssue = createdIssues.find( issue => issue.name === hour.key );
                if (!createdIssue) {
                    // check if issue is on Mantis
                    let mantisIssueId = await mantisRequester.isIssueCreated(hour.key, hour.title);
                    if (!mantisIssueId) {
                        mantisIssueId = await mantisRequester.reportIssue(hour);
                    }
                    createdIssue = { name: hour.key, issueId: mantisIssueId };
                    createdIssues.push(createdIssue);
                }
                // finally log the hour
                await reporteHorasRequester.reportHour(hour, createdIssue.issueId);
                console.log(`Correctly logged ${hour.date} - ${hour.key} - ${hour.time_spent} hours`)
                loggedHours += `-- ${hour.date} - ${hour.key} - ${hour.time_spent} hours\n`
            }
            catch (error) {
                errors += `-- ${hour.date} - ${hour.key} - ${hour.time_spent} hours - Error: ${error}\n\n`;
                console.error(`Issue ${hour.key} from ${hour.date} with ${hour.time_spent} hours couldn't be logged: ${error}`);
            }
        };
        if(!errors){
            return loggedHours;
        }
        throw new Error(`Couldn't log hours: \n${errors} \nFollowing hours were correctly logged: \n${loggedHours}`);
    }
}

