import cheerio from 'cheerio';

// parses issue search from Mantis and returns issue number if exists, if not returns empty
const parseIssueSearch = (html) => {

    try {
        let $ = cheerio.load(html);

        let firstResult = $('#buglist > tbody > tr:nth-child(4) td');
        return firstResult.attr('value');
    }
    catch(error){
        console.error(`Issue couldn't not be parsed: ${error}`);
        return null;
    }
}

export default parseIssueSearch;