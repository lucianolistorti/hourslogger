import cheerio from 'cheerio';

// parses token from set_project.php response for further use
const parseToken = (html) => {
    let $ = cheerio.load(html);

    let form = $('form [name="bug_report_token"]');
    return form.attr('value');
}

export default parseToken;