import cheerio from 'cheerio';

// parses RH modules and finds module id that corresponds to the Jira log hours.
const getModuleForLog = (html, epic, component, type) => {

    try {
        let $ = cheerio.load(html);

        let modules = $('#selModulos > option');

        let mantisName = '';
        if(component == 'Spike' || type == 'Spike') {
            mantisName = 'Spike';
        } else {
            mantisName = (epic) ? `EPIC - ${epic}` :   `OPERATIVE - ${component}`;
        }

        for (let key in modules) {
            const mantisModule = modules[key];
            if (mantisModule.firstChild.data === mantisName) return mantisModule.attribs.value;
        }

        throw new Error(`${mantisName} module was not found on select options.`)
    }
    catch (error) {
        throw error;
    }
}

export default getModuleForLog;