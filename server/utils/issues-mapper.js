import config from "../config.js";

const fieldsMap = {
    categoriesMap: {
        "Bug": "1810", // 1810: Bug Funcional
        "Task": "1817", // 1817: Task
        "Story": "1817",
        "Release": "1817",
        "Spike": "1817",
        "New Feature": "1817"
    },
    priorityMap: {
        "Lowest": "20",
        "Low": "20",
        "Medium": "30",
        "High": "40",
        "Highest": "50"
    }
}

const getCategory = (jiraCategory) => {
    return fieldsMap.categoriesMap[jiraCategory];
}

const getPriority = (jiraPriority) => {
    return fieldsMap.priorityMap[jiraPriority];
}

const getSummary = (jiraKey, jiraTitle) => {
    return `${jiraKey} - ${jiraTitle}`;
}

const getDescription = (jiraKey, jiraTitle) => {
    return `${jiraKey} - ${jiraTitle}`;
}

const gerComplexity = (jiraEstimate) => {
    jiraEstimate.replace('h','');
    jiraEstimate = parseInt(jiraEstimate) / 8; // map hours to days.

    // OPERATIVE task case
    if(jiraEstimate === 0) return "Normal";
    if(jiraEstimate <= 1) return "Muy baja";
    if(jiraEstimate <= 2) return "Baja";
    if(jiraEstimate <= 3) return "Normal";
    if(jiraEstimate <= 4) return "Alta";
    if(jiraEstimate > 4) return "Muy alta";

}

const mapJiraToMantisIssue = (jiraIssue) => {

    try {
        let mantisIssue = {
            bug_report_token: jiraIssue.report_token,
            m_id: '0',
            project_id: config.mantis.project_id,
            handler_id: '0',
            category_id: getCategory(jiraIssue.type),
            reproducibility: "70", // "have not tried"
            severity: "50", // "minor"
            priority: getPriority(jiraIssue.priority),
            summary: getSummary(jiraIssue.key, jiraIssue.title),
            description: getDescription(jiraIssue.key, jiraIssue.title),
            additional_info: "",
            custom_field_37: gerComplexity(jiraIssue.estimation),
            custom_field_35: "Reportado",
            custom_field_36: "",
            max_file_size: "2000000",
            view_state: "10",
            custom_field_3: "On Going"
        }

        return mantisIssue;
    }
    catch (error){
        throw new Error(`Couldn't map Jira issue to Mantis issue: ${error}`);
    }
}

export default mapJiraToMantisIssue;