import fs from 'fs/promises';
import parse from 'csv-parse/lib/sync.js';
import config from '../config.js';

const parseDataCsv = async (csvPath) => {
    let csvData = [];

    const fileContent = await fs.readFile(csvPath);
    fs.unlink(csvPath, (err) => {
        if (err) {
            console.error(err)
            return;
        }
        console.log('File deleted');
    })

    csvData = parse(fileContent, { columns: config.csv_columns, fromLine: 2 });
    csvData.pop(); // remove last line of "Total"

    return csvData;
}

export default parseDataCsv;