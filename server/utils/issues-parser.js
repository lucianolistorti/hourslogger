export const getIssueFromSearch = (cookies) => {

    let bugListCookie = cookies.split(';')[0];
    let bugList = bugListCookie.split('=')[1]
    bugList = bugList.split('%2C');

    return (bugList[0] === 'deleted') ? null :  bugList[0];
}

export const getIssueFromCreate = (createData) => {

    let regex = /View Submitted Issue (\d+)</;

    return createData.match(regex)[1];
}