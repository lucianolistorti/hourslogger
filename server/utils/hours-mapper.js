import config from '../config.js'

const getDate = (jiraDate) => {

    let date = new Date(jiraDate);

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    day = (day < 10) ? '0'+day : day;
    month = (month < 10) ? '0'+month : month;

    return `${year}-${month}-${day}`;
}

const getDescription = (jiraKey, jiraDescription) => {
    return `${jiraKey} - ${jiraDescription}`;
}

const getHours = (timeSpent) => {
    let floatTime = parseFloat(timeSpent).toFixed(2);
    return `${Math.floor(floatTime)}`;
}

const getMinutes = (timeSpent) => {
    let floatTime = parseFloat(timeSpent).toFixed(2);
    let minutes = (floatTime - Math.floor(floatTime)) * 60;
    return `${minutes}`;
}

const getDiscipline = (jiraType) => {
    if(jiraType === 'Spike') return '3' // Análisis
    return '2' // Construcción
}

const getTareaMantis = (issueId, jiraKey, jiraDescription) => {
    return `${issueId} - ${jiraKey} - ${jiraDescription}`;
}

const mapJiraToRHLog = (jiraHour) => {

    try {
        let rhLog = {
            idHoraReportada: '',
            campoOrden: '',
            sentido: '',
            txtFecha: getDate(jiraHour.date),
            txtTarea: getDescription(jiraHour.key, jiraHour.title),
            tareaMantis: getTareaMantis(jiraHour.issueId, jiraHour.key, jiraHour.title),
            selectHoras: getHours(jiraHour.time_spent),
            selectMinutos: getMinutes(jiraHour.time_spent),
            selTipoDisciplina: getDiscipline(jiraHour.type),
            selTipoHora: '1',
            selProyecto: config.reporte_horas.project_id,
            selModulos: jiraHour.module,
            selMantisTasks: jiraHour.issueId,
            selectedTaskId: '',
            grabar: 'Grabar',
            cantRegistros: '10'
        }

        return rhLog;
    }
    catch (error){
        throw new Error(`Couldn't map Jira hours to Reporte Horas hours: ${error}`);
    }
}

export default mapJiraToRHLog;