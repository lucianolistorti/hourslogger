import axios from 'axios';
import FormData from 'form-data';
import parseToken from '../html-parsers/token-parser.js';
import config from '../config.js'
import { getIssueFromSearch, getIssueFromCreate } from '../utils/issues-parser.js';
import mapJiraToMantisIssue from '../utils/issues-mapper.js';

export default class MantisRequester {
    constructor() {
        this.sessionCookies = '';
        this.reportToken = '';

    }

    getReportToken() {
        return this.reportToken;
    }

    // Checks on Mantis if issue is already created. If created return issue number.
    async isIssueCreated(key, title) {
        if (!this.sessionCookies) {
            this.sessionCookies = await this.login();
        }

        let formData = new FormData();
        formData.append('type', '1');
        formData.append('page_number', '1');
        formData.append('view_type', 'simple');
        formData.append('search', key);
        formData.append('filter', 'Apply Filter');

        let headers = formData.getHeaders();
        headers.Cookie = this.sessionCookies;

        let response = '';

        try {
            response = await axios.post(config.mantis.VIEW_ALL_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });
        } catch (error) {
            console.error(`Couldn't search issue on Mantis. ${error}`);
        }

        let issue = getIssueFromSearch(response.headers['set-cookie'][0]);
        return issue;
    }

    // login request and sets the sessionCookies to use on further requests.
    async login() {
        let formData = new FormData();
        formData.append('username', config.mantis.username);
        formData.append('password', config.mantis.password);

        let response = '';

        try {
            response = await axios.post(config.mantis.LOGIN_URL, formData, {
                headers: formData.getHeaders(),
                maxRedirects: 0,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });

            // parse cookies from login response and set it for further use
            let cookies = '';
            response.headers['set-cookie'].forEach(cookie => {
                cookie = cookie.split(';');
                cookies += cookie[0] + ';';
            });
            cookies += `MANTIS_PROJECT_COOKIE=${config.mantis.project_id}`;

            return cookies;
        } catch (error) {
            throw new Error(`Error logging to Mantis. ${error}`);
        }


    }

    // sets the project and parses the token from the html response.
    async setProject() {
        if (!this.sessionCookies) {
            this.sessionCookies = await this.login();
        }

        let formData = new FormData();

        let headers = formData.getHeaders();
        headers.Cookie = this.sessionCookies;

        let response = '';

        try {
            response = await axios.post(config.mantis.BUG_REPORT_PAGE_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });

            return parseToken(response.data);
        } catch (error) {
            throw new Error(`Error on bug_report_page.php request. ${error}`);
        }
    }

    async createIssue(issueData) {
        try {
            if (!this.sessionCookies) {
                this.sessionCookies = await this.login();
            }

            let formData = new FormData();
            for (let key in issueData) {
                formData.append(key, issueData[key]);
            }

            let headers = formData.getHeaders();
            headers.Cookie = `${this.sessionCookies}MANTIS_PROJECT_COOKIE=${config.mantis.project_id};`;

            let response = '';

            response = await axios.post(config.mantis.BUG_REPORT_URL, formData, {
                headers: headers,
                validateStatus: (status) => status >= 200 && status < 400 // do not fail with redirects
            });
            return getIssueFromCreate(response.data);
        } catch (error) {
            console.error(`Error on bug_report.php request. ${error}`);
        }

        console.log(`Issue ${issueData.summary} was created on Mantis.`);
    }

    // maps CSV to Mantis data
    getIssueData(data) {
        return mapJiraToMantisIssue(data);
    }

    async reportIssue(data) {

        try {
            if (!this.sessionCookies) {
                this.sessionCookies = await this.login();
            }

            this.reportToken = await this.setProject();

            data.report_token = this.reportToken;

            let issueData = this.getIssueData(data);

            return await this.createIssue(issueData);
        }
        catch (error) {
            throw error;
        }
    }
}
