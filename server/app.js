import config from './config.js'
import HoursLogger from './hours-logger.js';
import express from 'express';
import multer from 'multer';

const app = express();
const port = 3000;
const upload = multer({ dest: 'c:\\upload' });

app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())
//  Allow form-data parsing
app.use(upload.any());


let hoursLogger = new HoursLogger();

// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'https://reportehoras.temperies.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

app.post('/upload', upload.single('csv_file'), (req, res) => {
    hoursLogger.csvFile = req.files[0];
    res.send(200);
});

app.post('/hourslogger', async (req, res) => {
    try {
        let loggedHours = await hoursLogger.logHours(req.body.sessionCookie);
        res.status(200).send(loggedHours);
    }
    catch (error) {
        console.error(error);
        res.status(400).send(error.message);
    }
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
